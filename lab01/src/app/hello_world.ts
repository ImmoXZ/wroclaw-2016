import {Component} from 'angular2/core';
import {HiddenDispley} from './hiddendispley.component';

@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'hello-world',
//*ngIf=" yourName===pattern || pattern === '' || notification === ''"
  template: `
  <div class="panel-body">
    <div class="form-group">
      <h3>Enter name, pattern and notification, name and pattern should be same and notification should be not empty for ACTION</h3>
    </div>
    <hidden-displey *ngIf=" yourName===pattern && notification!=''" [notification]="notification" [name]="yourName">></hidden-displey>
    <div class="form-group">
      <label>Name:</label>
      <input type="text" [(ngModel)]="yourName" placeholder="Enter a name here">
     </div>
     <div class="form-group">
      <label>Pattern:</label>
      <input type="text" [(ngModel)]="pattern" placeholder="Enter a patern here">
     </div>
     <div class="form-group">
      <label>Notyfication:</label>
      <input type="text" [(ngModel)]="notification" placeholder="Enter a notification here">
      <hr>
     </div>
    <h1 [hidden]="!yourName">Hello {{yourName}}!</h1>
  </div>
`,
directives: [HiddenDispley]
})

export class HelloWorld {
  // Declaring the variable for binding with initial value
  yourName: string = '';
  pattern: string = '';
  notification: string = '';
}
