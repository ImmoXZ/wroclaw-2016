import {Component, Input} from 'angular2/core';

@Component({
	selector: 'hidden-displey',
	template: `
		<div>
			<h1>{{name}} {{notification}}</h1>
		</div>
	`
})


export class HiddenDispley {
  @Input()
  notification: string;
  @Input()
  name: string;
}
